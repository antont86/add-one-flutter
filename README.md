# Add one

A flutter application inspired by the cognitive test add one from Daniel Kahneman's book. 

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Building for Android and iOS

1. Build for Android: 
   - `flutter build apk`
2. Build for iOS:
   - `flutter build ios`

For more information see [docu](https://flutter.dev/docs/deployment/android).

## Building for web

See [docu for web.]([https://link](https://flutter.dev/docs/deployment/web))

### Note

When running `flutter run -d chrome` an issue can occur like *No devices detected with ID chrome*. 

In that case checkout your flutter branch to master and try again.

