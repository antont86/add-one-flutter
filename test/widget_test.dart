// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:add_one/playpage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:add_one/main.dart';
import 'package:mockito/mockito.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}


void main() {
  MockNavigatorObserver navigatorObserver;

  setUp(() {
    navigatorObserver = MockNavigatorObserver();
  });

  testWidgets('Counter increments smoke test', (WidgetTester tester) async {


    final rule = """Rule is simple:
  Increment each position of the number by one. E.g. 3417 the solution is 4527.
  That's it!""";

    final myHomePageWidgetCenteredTitle = find.widgetWithText(Center, 'Add One +1');
    final welcomeCenteredText = find.widgetWithText(Center, 'Welcome to Add One!');
    final ruleWidget = find.text(rule);
    final playBtnWidget = find.widgetWithText(FloatingActionButton, 'Play');



    // Build our app and trigger a frame.
    await tester.pumpWidget(
        MaterialApp(
            home: AddOne(),
            navigatorObservers: [navigatorObserver],
        ));
    expect(myHomePageWidgetCenteredTitle, findsOneWidget);
    expect(welcomeCenteredText, findsOneWidget);
    expect(ruleWidget, findsOneWidget);
    expect(playBtnWidget, findsOneWidget);

    await tester.tap(playBtnWidget);
    await tester.pumpAndSettle();

    expect(find.byType(PlayPage), findsOneWidget);
  });
}


