import 'package:add_one/playpage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:mockito/mockito.dart';


void main() {
  testWidgets('Show text field with random number between 0 and 9999, also an input field', (WidgetTester tester) async {

    final randomNumber = find.byKey(Key('randomNumber'));

    await tester.pumpWidget(
        MaterialApp(
          home: PlayPage()));

    await tester.pumpAndSettle();

    final randomNumberText = (find.byKey(Key('randomNumber')).evaluate().single.widget as Text).data;
    final incrementedNumberFormField = find.byKey(Key('incrementedNumber'));

    expect(randomNumber, findsOneWidget);
    expect(incrementedNumberFormField, findsOneWidget);

    expect(int.parse(randomNumberText), greaterThanOrEqualTo(0));
    expect(int.parse(randomNumberText), lessThanOrEqualTo(9999));
  });


  test('Each position of the given number each position must be incremented', () {
    final _playPage = PlayPage().createState();

    String incrementedNumber = _playPage.incrementNumberBy('2222', 1);

    expect(incrementedNumber, equals('3333'));
    incrementedNumber = _playPage.incrementNumberBy('2902', 1);
    expect(incrementedNumber, equals('3013'));
    incrementedNumber = _playPage.incrementNumberBy('0909', 1);
    expect(incrementedNumber, equals('1010'));
    incrementedNumber = _playPage.incrementNumberBy('8990', 1);
    expect(incrementedNumber, equals('9001'));
    incrementedNumber = _playPage.incrementNumberBy('9999', 1);
    expect(incrementedNumber, equals('0000'));
    incrementedNumber = _playPage.incrementNumberBy('2472', 1);
    expect(incrementedNumber, equals('3583'));
  });

  test('Compare numbers', () {
    expect(IncrementedNumberUtils().incrementNumberBy("2222", 1), equals("3333"));
    expect(IncrementedNumberUtils().compareNumbers("3333", "2222"), equals(true));
  });

  testWidgets('Random generated number in the textfield should not change after focus on input', (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(
            home: PlayPage()));
    await tester.pumpAndSettle();
    final String randomGeneratedNumber = (find.byKey(Key('randomNumber')).evaluate().single.widget as Text).data;

    expect(randomGeneratedNumber.isNotEmpty, true);
    await tester.tap(find.byKey(Key('incrementedNumber')));
    await tester.pumpWidget(
        MaterialApp(
            home: PlayPage()));
    String randomGeneratedNumberShouldNotChange = (find.byKey(Key('randomNumber')).evaluate().single.widget as Text).data;

    expect(randomGeneratedNumber, equals(randomGeneratedNumberShouldNotChange));

  });

  testWidgets('Show error if input is not correct and display a snackbar error', (WidgetTester tester) async {
    MockUtils utils = new MockUtils();
    await tester.pumpWidget(
        MaterialApp(
            home: PlayPage()));


    final String randomGeneratedNumber = (find.byKey(Key('randomNumber')).evaluate().single.widget as Text).data;
    await tester.enterText(find.byKey(Key('incrementedNumber')), randomGeneratedNumber);
    await tester.pumpAndSettle();
    final nextButton = find.byKey(Key('nextNumber'));
    expect(nextButton, findsOneWidget);
    await tester.tap(nextButton);
    await tester.pumpAndSettle();
    final snackBarFinder = find.byType(SnackBar);
    verify(utils.compareNumbers(randomGeneratedNumber, randomGeneratedNumber)).called(1);
    final snackBarActionFinder = find.byType(SnackBarAction);

    expect(snackBarFinder, findsOneWidget);
    expect(snackBarActionFinder, findsNothing);

    expect(find.text("Wrong answer"), findsOneWidget);

  });

  testWidgets('Show error if input is  correct and display a snackbar success msg', (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(
            home: PlayPage()));


    final String randomGeneratedNumber = (find.byKey(Key('randomNumber')).evaluate().single.widget as Text).data;
    await tester.enterText(find.byKey(Key('incrementedNumber')), IncrementedNumberUtils().incrementNumberBy(randomGeneratedNumber, 1));
    await tester.pumpAndSettle();
    final nextButton = find.byKey(Key('nextNumber'));
    await tester.tap(nextButton);
    await tester.pumpAndSettle();
    final snackBarFinder = find.byType(SnackBar);
    final snackBarActionFinder = find.byType(SnackBarAction);

    expect(snackBarActionFinder, findsOneWidget);

    expect(snackBarFinder, findsOneWidget);

    expect(find.text("Correct answer"), findsOneWidget);
    expect(find.text("Continue"), findsOneWidget);

  });

  testWidgets('Show error if input is not correct and display a snackbar error', (WidgetTester tester) async {
    final playPlage = new PlayPage();

    await tester.pumpWidget(
        MaterialApp(
            home: playPlage));

    final PlayPageState state = tester.state(find.byType(PlayPage));


    final String randomGeneratedNumber = (find.byKey(Key('randomNumber')).evaluate().single.widget as Text).data;
    await tester.enterText(find.byKey(Key('incrementedNumber')), randomGeneratedNumber);
    await tester.pumpAndSettle();
    final nextButton = find.byKey(Key('nextNumber'));
    expect(nextButton, findsOneWidget);
    expect(state.i, 1);
    await tester.tap(nextButton);

    expect(state.i, equals(3));

  });

  testWidgets('Generate new number after successfully answering', (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(
            home: PlayPage()));

    final String displayedNumber = (find.byKey(Key('randomNumber')).evaluate().single.widget as Text).data;
    await tester.enterText(find.byKey(Key('incrementedNumber')), IncrementedNumberUtils().incrementNumberBy(displayedNumber, 1));
    final nextButton = find.byKey(Key('nextNumber'));
    await tester.tap(nextButton);
    await tester.pumpAndSettle();

    final snackBarActionFinder = find.byType(SnackBarAction);
    await tester.tap(snackBarActionFinder);
    await tester.pumpAndSettle();
    final String newDisplayedNumber = (find.byKey(Key('randomNumber')).evaluate().single.widget as Text).data;
    expect(displayedNumber, isNot(newDisplayedNumber));

    final String inputNumber = (find.byKey(Key('incrementedNumber')).evaluate().single.widget as TextFormField).controller.text;
    expect(inputNumber, isEmpty);

  });
  
  
  testWidgets('Autofocus on keyboard after starting the game', (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(
            home: PlayPage()));


  });
}

class MockUtils extends Mock implements IncrementedNumberUtils {}

