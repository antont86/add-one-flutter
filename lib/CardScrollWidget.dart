import 'package:add_one/SettingsEnum.dart';
import 'package:add_one/playpage.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'data.dart';
import 'dart:math';

class CardScrollWidget extends StatelessWidget {
  var currentPage;
  var padding = 20.0;
  var verticalInset = 20.0;
  var cardAspectRatio = 12.0 / 22.0;
  var widgetAspectRatio = (12.0 / 22.0) * 1.2;

  CardScrollWidget(this.currentPage);

  @override
  Widget build(BuildContext context) {
    return new AspectRatio(
      aspectRatio: widgetAspectRatio,
      child: LayoutBuilder(builder: (context, contraints) {
        List<Widget> cardList = new List();

        List<Widget> iconList = new List();
        iconList.add(Icon(Icons.settings, size: 100, color: Colors.white));

        iconList.add(Icon(Icons.games, size: 100, color: Colors.white));

        for (var i = 0; i < images.length; i++) {
          var delta = i - currentPage;

          var cardItem = Positioned.directional(
            top: padding + verticalInset * max(-delta, 0.0),
            bottom: padding + verticalInset * max(-delta, 0.0),
            start: 0,
            textDirection: TextDirection.rtl,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16.0),
              child: Container(
                child: AspectRatio(
                  aspectRatio: cardAspectRatio,
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: [Colors.blue, Colors.red]),
                        ),
                      ),
                      iconList[i],
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 0.0, bottom: 12.0),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 6.0),
                                child: FlatButton.icon(
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(20.0)),
                                    color: Colors.blueAccent,
                                    icon: Icon(Icons.play_circle_outline),
                                    //`Icon` to display
                                    label: Text(title[i]),
                                    //`Text` to display
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => PlayPage()),
                                      );
                                    }),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
          cardList.add(cardItem);
        }
        return Stack(
          children: cardList,
        );
      }),
    );
  }
}

class CardScrollSingleWidget extends StatelessWidget {
  var currentPage = 1;
  var navigateTo;
  var padding = 20.0;
  var verticalInset = 20.0;
  var cardAspectRatio = 12.0 / 10.0;
  var widgetAspectRatio = (12.0 / 22.0);

  var index;

  CardScrollSingleWidget(this.index);

  @override
  Widget build(BuildContext context) {
    return new AspectRatio(
      aspectRatio: widgetAspectRatio,
      child: LayoutBuilder(builder: (context, contraints) {
        List<Widget> iconList = new List();
        iconList.add(Icon(Icons.settings, size: 100, color: Colors.white));

        iconList.add(Icon(Icons.games, size: 100, color: Colors.white));

        var cardItem = ClipRRect(
          borderRadius: BorderRadius.circular(0),
          child: Container(
            child: AspectRatio(
              aspectRatio: cardAspectRatio,
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [Colors.blue, Colors.red]),
                    ),
                  ),
                  iconList[index],
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.only(left: 0.0, bottom: 12.0),
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 14.0, vertical: 6.0),
                            child: FlatButton.icon(
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(20.0)),
                                color: Colors.blueAccent,
                                icon: Icon(Icons.play_circle_outline),
                                //`Icon` to display
                                label: Text(title[index]),
                                //`Text` to display
                                onPressed: () {
                                  if (true) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => PlayPage()),
                                    );
                                  }
                                }),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

        return cardItem;
      }),
    );
  }
}

class MenuItem extends StatefulWidget {
  Widget icon;
  Widget navigate;
  String title;

  AnimationController controller;

  MenuItem(Widget this.icon, Widget this.navigate, this.title);

  @override
  MenuItemState createState() => MenuItemState();
}

class MenuItemState extends State<MenuItem>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;
  SharedPreferences prefs;
  int incrementValue = 1;

  showOverlay(BuildContext context) {
    OverlayState overlayState = Overlay.of(context);
    OverlayEntry overlayEntry =
        OverlayEntry(builder: (context) => Positioned(
          bottom: 10.0,
          right: 10.0,
          child: Text('Sws')
        ));

    overlayState.insert(overlayEntry);
  }

  @override
  void initState() {
    super.initState();
    initSharedPrefs();
    controller = new AnimationController(
        duration: Duration(milliseconds: 3000), vsync: this);
    animation = new Tween<double>(begin: 0.0, end: 3.0).animate(controller);

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        new Future.delayed(const Duration(seconds: 1), () {
          if (!mounted) return;
          controller?.forward();
        });
      }
    });
    controller.forward();
  }

  void initSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      incrementValue = prefs.getInt(SettingsEnum.INCREMENT_VALUE.toString());
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Container(
              height: 250,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [Colors.red, Colors.blue]),
              ),
              child: SizedBox.expand(
                  child: Center(
                      child: Text(
                'Add One +$incrementValue',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 40,
                ),
              )))),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTile(
                    leading: Icon(Icons.album),
                    title: Text('How to'),
                    subtitle: Text(
                        'Increment each position of the number by $incrementValue. E.g. if the shown number is 2345 then add $incrementValue to \"2\", \"3\", \"4\", \"5\". '
                        'The result would be ${2 + incrementValue}${3 + incrementValue}${4 + incrementValue}${5 + incrementValue}.')),
                ButtonBarTheme(
                  data: ButtonBarThemeData(alignment: MainAxisAlignment.center),
                  // make buttons use the appropriate styles for cards
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton.icon(
                        icon: AnimatedIcon(
                            icon: AnimatedIcons.play_pause,
                            progress: animation),
                        label: const Text('Start game'),
                        onPressed: () {
                          if (widget.navigate != null) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => widget.navigate),
                            );
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 200,
            child:
              Align(
                alignment: Alignment.bottomRight,
                child: Image(image: AssetImage('assets/swipeforsettings.png'), width: 200,)
              ),
                      )
        ],
      ),
    );
  }
}
