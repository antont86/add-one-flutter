import 'dart:math';

import 'package:add_one/SettingsEnum.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PlayPage extends StatefulWidget {

  final IncrementedNumberUtils incrementedUtils = IncrementedNumberUtils();

  @override
  PlayPageState createState() => PlayPageState();
}

class PlayPageState extends State<PlayPage> {
  int i = 1;
  final _random = new Random();

  SnackBar snackBar;

  SharedPreferences prefs;

  int inputLength;
  int incrementValue;
  double lengthOfInput = 40;

  String generatedNumber;
  TextEditingController incrementedNumberController =
      new TextEditingController();


  String randomNumber(int length) {
    int maxNumber = 9999;
    switch(length) {
      case 4: {
        maxNumber = 9999;
      }
      break;
      case 5: {
        maxNumber = 99999;
      }
      break;
      case 6: {
        maxNumber = 999999;
      }
      break;
      case 7: {
        maxNumber = 9999999;
      }
      break;
      default: {
        maxNumber = 9999;
      }
    }

    return _random.nextInt(maxNumber).toString().padLeft(length, '0');
  }

  String incrementNumberBy(String number, int increment) {
    List numberAsList = number.split('');
    List incrementedNumberAsList = List();
    int subTenIfValueIsTen = 0;
    numberAsList.forEach((number) {
      subTenIfValueIsTen = 0;
      if (int.parse(number) == 9) {
        subTenIfValueIsTen = 10;
      }

      incrementedNumberAsList.add(int.parse(number) + 1 - subTenIfValueIsTen);
    });
    return incrementedNumberAsList.join('');
  }

  @override
  void initState() {
    super.initState();
    initPlayPageState();


  }

  void initPlayPageState() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      inputLength = prefs.getInt(SettingsEnum.LENGTH.toString());
      incrementValue = prefs.getInt(SettingsEnum.INCREMENT_VALUE.toString());
      lengthOfInput = 10.toDouble() * inputLength;
      this.generatedNumber = randomNumber(inputLength);



    });
  }

  void showSnackBarMessage(BuildContext context, bool success) {
    if (success) {
      snackBar = SnackBar(
          content: Text("Correct answer"),
          action: SnackBarAction(
              label: 'Continue',
              onPressed: () {
                setState(() {
                  this.generatedNumber = randomNumber(inputLength);
                  incrementedNumberController.clear();
                });
              }));
    } else {
      snackBar = SnackBar(content: Text("Wrong answer"));
    }

    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: Text('Add One')),
        ),
        body: Builder(
          builder: (context) => Center(
              child: Column(
            children: <Widget>[
              Flexible(
                  fit: FlexFit.tight,
                  flex: 2,
                  child: Center(
                    child: Container(
                        child: Text(this.generatedNumber.toString(),
                            key: Key('randomNumber'),
                            style: Theme.of(context).textTheme.display1)),
                  )),
              Flexible(
                  flex: 4,
                  fit: FlexFit.tight,
                    child: Container(
                      width: 60 + lengthOfInput,
                      child: TextFormField(
                        controller: incrementedNumberController,
                        key: Key('incrementedNumber'),
                        keyboardType: TextInputType.number,
                        autofocus: true,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 35),
                        maxLength: inputLength,
                        keyboardAppearance: Brightness.light,

                      ),
                    ),
                  ),
              Flexible(
                  flex: 2,
                  fit: FlexFit.loose,
                  child: Container(
                        margin: EdgeInsets.only(bottom: 50),
                        child: RaisedButton(
                            child: Text('Check number'),
                            color: Colors.deepPurpleAccent,
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(18.0),
                            ),
                            key: Key('nextNumber'),
                            elevation: 3,
                            onPressed: () {
                              bool success = widget.incrementedUtils
                                  .compareNumbers(
                                  incrementedNumberController.text,
                                      IncrementedNumberUtils().incrementNumberBy(this.generatedNumber, incrementValue));
                              success = IncrementedNumberUtils().compareNumbers(
                                  incrementedNumberController.text,
                                  IncrementedNumberUtils().incrementNumberBy(this.generatedNumber, incrementValue));

                              showSnackBarMessage(context, success);
                            }),
                      ))
            ],
          )),
        ));
  }
}

class IncrementedNumberUtils {
  String incrementNumberBy(String number, int increment) {
    List numberAsList = number.split('');
    List incrementedNumberAsList = List();
    int subTenIfValueIsTen = 0;
    numberAsList.forEach((number) {
      subTenIfValueIsTen = 0;
      if (int.parse(number) >= (10 - increment)) {
        subTenIfValueIsTen = 10;
      }

      incrementedNumberAsList.add(int.parse(number) + increment - subTenIfValueIsTen);
    });
    return incrementedNumberAsList.join('');
  }

  bool compareNumbers(String number, String toBeIncrementedNumber) {
    return toBeIncrementedNumber.compareTo(number) == 0;
  }
}
