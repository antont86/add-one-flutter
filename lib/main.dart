import 'package:add_one/SettingsEnum.dart';
import 'package:add_one/playpage.dart';
import 'package:flutter/material.dart';
import 'CardScrollWidget.dart';
import 'Settings.dart';
import 'data.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(AddOne());

class AddOne extends StatelessWidget {
  // This widget is the root of your application.

  SharedPreferences prefs;

  @override
  Widget build(BuildContext context) {
    initSharedPrefs();

    return MaterialApp(
      home: MyHomePage(title: 'Add One +1'),
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      )
    );
  }

  void initSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();


    if(prefs.getInt(SettingsEnum.LENGTH.toString()) == null) {
      prefs.setInt(SettingsEnum.LENGTH.toString(), 4);
    }

    if(prefs.getInt(SettingsEnum.INCREMENT_VALUE.toString()) == null) {
      prefs.setInt(SettingsEnum.INCREMENT_VALUE.toString(), 1);
    }

  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var currentPage = images.length - 1.0;

  final rule = """Rule is simple:
  Increment each position of the number by one. E.g. 3417 the solution is 4527.
  That's it!""";



  @override
  Widget build(BuildContext context) {

    List<Widget> iconList = new List();
    iconList.add(Icon(Icons.settings, size: 100, color: Colors.white));

    iconList.add(Icon(Icons.games, size: 100, color: Colors.white));


    PageController controller = PageController(initialPage: images.length - 1);
    controller.addListener(() {
      setState(() {
        currentPage = controller.page;
      });
    });

    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
                Color(0xFF1b1e44),
                Color(0xFF2d3447),
              ],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              tileMode: TileMode.clamp)),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
          child:
              PageView(
                children: <Widget>[
                  Container(
                    child: MenuItem(iconList[0], PlayPage(), 'Main'),
                    color: Colors.white,
                  ),
                  Container(
                    child: Settings(),
                    color: Colors.white,
                  ),

                ],
                scrollDirection: Axis.horizontal,
              ),


        )
      ),
    );
  }
}

