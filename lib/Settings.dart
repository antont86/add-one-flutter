import 'package:add_one/SettingsEnum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';



class Settings extends StatefulWidget {
  @override
  SettingsState createState() => SettingsState();
}

class SettingsState extends State<Settings>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  SharedPreferences prefs;

  int lengthOfNumber;
  int incrementValue;

  @override
  void initState() {
    super.initState();
    controller = new AnimationController(
        duration: Duration(milliseconds: 3000), vsync: this);
    animation = new Tween<double>(begin: 0.0, end: 3.0).animate(controller);

    initSharedPrefs();


    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        new Future.delayed(const Duration(seconds: 1), () {
          if (!mounted) return;
          controller?.forward();
        });
      }
    });
    controller.forward();
  }

  void initSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      lengthOfNumber = prefs.getInt(SettingsEnum.LENGTH.toString());
      incrementValue = prefs.getInt(SettingsEnum.INCREMENT_VALUE.toString());
    });


  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Row(
                children: <Widget> [
                  Icon(Icons.settings, size: 30),
                  Text('Settings', style: TextStyle(fontSize: 30),)
                ]
            )
          ),
          Container(
              height: 20,
              decoration: BoxDecoration(),
              child: SizedBox.expand()),
          Container(
            child: Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const ListTile(
                      leading: Icon(Icons.album),
                      title: Text('Length'),
                      subtitle:
                          Text('Set length of shown number.')),
                  DropdownButton<int>(
                    value: lengthOfNumber,
                    icon: Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    style: TextStyle(
                        color: Colors.deepPurple
                    ),
                    underline: Container(
                      height: 2,
                      color: Colors.deepPurpleAccent,
                    ),
                    onChanged: (int newValue) {
                      setState(() {
                        lengthOfNumber = newValue;
                      });
                        prefs.setInt(SettingsEnum.LENGTH.toString(), newValue);

                    },
                    items: <int>[4, 5, 6, 7]
                        .map<DropdownMenuItem<int>>((int value) {
                      return DropdownMenuItem<int>(
                        value: value,
                        child: Text(value.toString()),
                      );
                    })
                        .toList(),
                  ),

                  const ListTile(
                      leading: Icon(Icons.lens),
                      title: Text('Increment'),
                      subtitle:
                      Text('Set the increment value. It is the value to be added to each position of the shown number.')),
                  DropdownButton<int>(
                    value: incrementValue,
                    icon: Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    style: TextStyle(
                        color: Colors.deepPurple
                    ),
                    underline: Container(
                      height: 2,
                      color: Colors.deepPurpleAccent,
                    ),
                    onChanged: (int newValue) {
                      setState(() {
                        incrementValue = newValue;
                        prefs.setInt(SettingsEnum.INCREMENT_VALUE.toString(), newValue);
                      });
                    },
                    items: <int>[1, 2, 3, 4]
                        .map<DropdownMenuItem<int>>((int value) {
                      return DropdownMenuItem<int>(
                        value: value,
                        child: Text(value.toString()),
                      );
                    })
                        .toList(),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
